#
# Compiler Setting
#
#include Mkinclude/Mkinclude.GNU
include Mkinclude/Mkinclude.Intel
#include Mkinclude/Mkinclude.PGI


LIB  = lib/libmusage.a
OBJS = src/InterFace.o

#
# Additional compiler/linker flags
#
FFLAGS   += -g
CFLAGS   += -g -Iinclude
CXXFLAGS += -g -Iinclude



#
# actual build command
#
all:$(LIB) f_sample cpp_sample c_sample

$(LIB):$(OBJS)
	-mkdir lib
	ar rv $(LIB) $(OBJS)

f_sample: sample/f_sample.o
	-mkdir bin
	$(FC) $(FFLAGS) sample/f_sample.o $(LIB) -o bin/f_sample -lstdc++

c_sample: sample/c_sample.o
	-mkdir bin
	$(CC) $(CFLAGS) sample/c_sample.o $(LIB) -o bin/c_sample -lstdc++

cpp_sample: sample/cpp_sample.o
	-mkdir bin
	$(CXX) $(CXXFLAGS) sample/cpp_sample.o $(LIB) -o bin/cpp_sample

clean:
	-rm -rf *.o $(TARGET) $(OBJS) $(LIB)

.PHONY: all clean

#
# suffix rules
#
.SUFFIXES: .f90 .F90
.f90.o:
	$(FC)  $(FFLAGS)   -c -o$@ $<
.F90.o:
	$(FC)  $(FFLAGS)   -c -o$@ $<
.C.o:
	$(CXX) $(CXXFLAGS) -c -o$@ $<

