#ifndef MEMORY_USAGE_H
#define MEMORY_USAGE_H

#ifdef __cplusplus 
#include <sys/types.h> 
#include <unistd.h>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <cstdlib>

namespace MemoryUsage
{
class MemoryUsage
{
    private:
        MemoryUsage(){}
        ~MemoryUsage(){}
        MemoryUsage(const MemoryUsage& obj);
        MemoryUsage& operator=(const MemoryUsage& obj);

    public:
        static MemoryUsage& get_instance()
        {
            static MemoryUsage instance;
            return instance;
        }

        /// @brief 自プロセスのHWM(プロセス生成以降のメモリ使用量の最大値)を返す
        //  返り値の単位はkB
        //  fs/proc/task_mmu.c 内でハードコーディングされている(Linux kernel 2.6)
        long get_HWM(void)
        {
            return read_stat("HWM");
        }

        /// 自プロセスの現時点でのメモリ使用量を返す
        long get_RSS(void)
        {
            return read_stat("RSS");
        }

    private:
        std::string get_status_file_name(void)
        {
            pid_t pid=getpid();
            std::ostringstream StatusFileName;
            StatusFileName <<"/proc/"<<pid<<"/status";
            return StatusFileName.str();
        }

        /// @brief /proc/{PID}/statusを読んで引数で指定された値を返す関数
        long read_stat(const char * keyword)
        {
            std::string StatusFileName(get_status_file_name());
            std::ifstream stat(StatusFileName.c_str());
            static std::string numbers("1234567890");
            std::string result("-1");

            std::string inputline;
            // keywordを含む行を見つけるまで読み飛ばしていって、あたったら":"以降の値を返す。
            while(!stat.eof())
            {
                getline(stat, inputline);
                std::string::size_type n = inputline.find(keyword);
                if( n != std::string::npos){
                    //found
                    std::string::size_type start = inputline.find_first_of(numbers, n);
                    std::string::size_type end   = inputline.find(" ", start);
                    result = inputline.substr(start, end);
                    stat.close();
                    break;
                }
            }
            return std::atol(result.c_str());
        }
};

//
//  interface method for C++
//
//! @brief 自プロセスのHWM(プロセス生成以降のメモリ使用量の最大値)を返す
//  返り値の単位はkB
//  fs/proc/task_mmu.c 内でハードコーディングされている(Linux kernel 2.6)
long get_HWM(void)
{
    return MemoryUsage::get_instance().get_HWM();
}

//! @brief 自プロセスの現時点でのメモリ使用量を返す
long get_RSS(void)
{
    return MemoryUsage::get_instance().get_RSS();
}

}//end of namespace MemoryUsage

extern "C" {
#endif
  long get_hwm(void);
  long get_rss(void);
#ifdef __cplusplus 
}
#endif
#endif
