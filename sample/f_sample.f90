program sample
implicit none
integer(kind=4), allocatable :: array(:)
interface
  integer(kind=8) function get_rss()
  end function get_rss
  integer(kind=8) function get_hwm()
  end function get_hwm
end interface

write(0,*) '*** Sample program for MemoryUsage start ***'
write(0,*) 'allocated memory is     ', get_rss(), 'kB'
write(0,*) 'max allocated memory is ',get_hwm() , 'kB'
write(0,*) ''

allocate(array(400*1024))
write(0,*) 'allocate 1600kB'
array=1

write(0,*) 'allocated memory is     ', get_rss(), 'kB'
write(0,*) 'max allocated memory is ',get_hwm() , 'kB'
write(0,*) ''

deallocate(array)
write(0,*) 'deallocate 1600kB'
write(0,*) 'allocated memory is     ', get_rss(), 'kB'
write(0,*) 'max allocated memory is ',get_hwm() , 'kB'
write(0,*) ''

allocate(array(200*1024))
write(0,*) 'allocate 800kB'
write(0,*) 'allocated memory is     ', get_rss(), 'kB'
write(0,*) 'max allocated memory is ',get_hwm() , 'kB'
write(0,*) ''
array=2

deallocate(array)
write(0,*) 'deallocate 800kB'
write(0,*) 'allocated memory is     ', get_rss(), 'kB'
write(0,*) 'max allocated memory is ',get_hwm() , 'kB'

end program
