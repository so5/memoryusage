#include <iostream>
#include "MemoryUsage.h"

int main(int argc,char *argv[]){
  int *array;
  int i;

  std::cerr<< "*** Sample program for MemoryUsage start ***\n"<<std::endl;
  std::cerr<< "allocated memory size     = "<<MemoryUsage::get_RSS()<<"\n"; 
  std::cerr<< "max allocated memory size = "<<MemoryUsage::get_HWM()<<"\n\n"; 

  array = new int [400*1024];
  std::cerr << "allocate 1600kB"<<"\n";
  for(i=0;i<400*1024;i++)
  {
    array[i]=i;
  }
  std::cerr<< "allocated memory size     = "<<MemoryUsage::get_RSS()<<"\n"; 
  std::cerr<< "max allocated memory size = "<<MemoryUsage::get_HWM()<<"\n\n"; 


  delete array;
  std::cerr<< "delete 1600kB"<<"\n";

  std::cerr<< "allocated memory size     = "<<MemoryUsage::get_RSS()<<"\n"; 
  std::cerr<< "max allocated memory size = "<<MemoryUsage::get_HWM()<<"\n\n"; 

  array = new int [200*1024];
  std::cerr << "allocate 800kB"<<"\n";
  for(i=0;i<200*1024;i++)
  {
    array[i]=i*2;
  }

  std::cerr<< "allocated memory size     = "<<MemoryUsage::get_RSS()<<"\n"; 
  std::cerr<< "max allocated memory size = "<<MemoryUsage::get_HWM()<<"\n\n"; 

  delete array;
  std::cerr<< "delete 800kB"<<"\n";

  std::cerr<< "allocated memory size     = "<<MemoryUsage::get_RSS()<<"\n"; 
  std::cerr<< "max allocated memory size = "<<MemoryUsage::get_HWM()<<"\n\n"; 
  return 0;
}
