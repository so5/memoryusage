#include <stdio.h>
#include <stdlib.h>
#include "MemoryUsage.h"

int main(int argc,char *argv[]){
  int *array;
  int i;

  fprintf(stderr, "*** Sample program for MemoryUsage start ***\n");
  fprintf(stderr, "allocated memory size     = %ld\n", get_rss());
  fprintf(stderr, "max allocated memory size = %ld\n\n", get_hwm());

  array = (int *) malloc(400*1024*sizeof(int));
  fprintf(stderr, "allocate 1600kB\n");
  for(i=0;i<400*1024;i++)
  {
    array[i]=i;
  }
  fprintf(stderr, "allocated memory size     = %ld\n", get_rss());
  fprintf(stderr, "max allocated memory size = %ld\n\n", get_hwm());


  free(array);
  fprintf(stderr, "free 1600kB\n");

  fprintf(stderr, "allocated memory size     = %ld\n", get_rss());
  fprintf(stderr, "max allocated memory size = %ld\n\n", get_hwm());

  array = (int *) malloc(200*1024*sizeof(int));
  fprintf(stderr, "allocate 800kB\n");
  for(i=0;i<200*1024;i++)
  {
    array[i]=i*2;
  }

  fprintf(stderr, "allocated memory size     = %ld\n", get_rss());
  fprintf(stderr, "max allocated memory size = %ld\n\n", get_hwm());

  free(array);
  fprintf(stderr, "free 800kB\n");

  fprintf(stderr, "allocated memory size     = %ld\n", get_rss());
  fprintf(stderr, "max allocated memory size = %ld\n\n", get_hwm());
  return 0;
}
