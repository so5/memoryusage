#include "MemoryUsage.h"

//
// interface method for C and Fortran
//
extern "C"
{
  long get_hwm_(void);
  long get_rss_(void);
}


long get_hwm(void)
{
    return MemoryUsage::get_HWM();
}
long get_hwm_(void)
{
    return MemoryUsage::get_HWM();
}
long get_rss(void)
{
    return MemoryUsage::get_RSS();
}
long get_rss_(void)
{
    return MemoryUsage::get_RSS();
}
