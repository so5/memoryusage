######################################################################
#
#  Compiler and compile option setting for GNU Compiler Collection
#
######################################################################
FC       = gfortran
CC       = gcc
CXX      = g++

FFLAGS   = -g -O2 -fbacktrace
CFLAGS  = -g -O2 
CXXFLAGS = $(CFLAGS)
LINKER   = $(CXX)
LDFLAGS  = 
AR       = ar rc
